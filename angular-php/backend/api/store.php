<?php
require 'connect.php';

// Get the posted data.
$postdata = file_get_contents("php://input");

if(isset($postdata) && !empty($postdata))
{
  // Extract the data.
  $request = json_decode($postdata);
	

  // Validate.
  if(trim($request->name) === '' && (string)$request->email==='')
  {
    return http_response_code(400);
  }
	
  // Sanitize.
  $username = mysqli_real_escape_string($con, trim($request->name));
  $email = mysqli_real_escape_string($con, (string)$request->email);
  $password = base64_encode(mysqli_real_escape_string($con, (string)$request->password));
  $gender = mysqli_real_escape_string($con, (string)$request->gender);
  $techstack = implode(',',$request->techstack); 

  // Store.
    $sql = "INSERT INTO `users`(`username`,`email`,`password`,`gender`,`techstack`) VALUES ('{$username}','{$email}','{$password}','{$gender}','{$techstack}')";
  
  
  if(mysqli_query($con,$sql))
  {
    http_response_code(201);
    $user = [
      'username' => $username,
      'email' => $email,
      'id'    => mysqli_insert_id($con)
    ];
    echo json_encode(['data'=>$user]);
  }
  else
  {
    http_response_code(422);
  }
}
