<?php
/**
 * Returns the list of cars.
 */
require 'connect.php';
    
$cars = [];
$sql = "SELECT * FROM users";

if($result = mysqli_query($con,$sql))
{
  $cr = 0;
  while($row = mysqli_fetch_assoc($result))
  {
    $users[$cr]['username']    = $row['username'];
    $users[$cr]['email'] = $row['email'];
    $users[$cr]['gender'] = $row['gender'];
    $users[$cr]['techstack'] = $row['techstack'];
    $cr++;
  }
    
  echo json_encode(['data'=>$users]);
}
else
{
  http_response_code(404);
}
