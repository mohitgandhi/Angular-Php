<?php
require 'connect.php';

// Get the posted data.
$postdata = file_get_contents("php://input");
if(isset($postdata) && !empty($postdata))
{
  // Extract the data.
  $request = json_decode($postdata);
  // Validate.
  if(trim($request->email) == '' || trim($request->password)=='')
  {
    return;
  }
    
  // Sanitize.
  $uemail   = mysqli_real_escape_string($con, trim($request->email));
  $upassword   = base64_encode(mysqli_real_escape_string($con, trim($request->password)));

  // Get by id.
  $sql = "SELECT * FROM `users` WHERE `email` ='{$uemail}' &&  `password` ='{$upassword}'";
  $result = mysqli_query($con,$sql);
  $row = mysqli_fetch_assoc($result);
  if(count($row)>0){
    $authToken=md5(uniqid(rand(), true));
    $sql = "UPDATE `users` SET `token`='{$authToken}' WHERE `email` ='{$uemail}'";

  if(mysqli_query($con, $sql))
  {
    $user = [
      'username' => $uemail,
      'token'    => $authToken
    ];
    echo json_encode($user);
  }
  else
  {
    return http_response_code(422);
  }  
  }
  
}
exit;