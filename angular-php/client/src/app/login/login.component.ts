import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AbstractControl, ValidatorFn,FormBuilder, FormGroup, Validators,FormArray,FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { AuthenticationService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService : AuthenticationService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', {
        validators: [Validators.required,
        Validators.email,
        Validators.maxLength(50)
        ],
        updateOn: 'blur'
      }),
      password: ['', Validators.required]
    });

  }


    // for accessing to form fields
    get fval() { return this.loginForm.controls; }

  onFormSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
     this.authenticationService.login(this.fval.email.value, this.fval.password.value)
        .subscribe(
            data => {
              if(data==null){
                //this.router.navigate(['/login']);
                this.loading = false;
                this.toastr.error('Incorrect Username or Password', 'Error');
              }else{
                this.router.navigate(['/']);
              }
            },
            error => {
              this.toastr.error(error.error.message, 'Error');
                this.loading = false;
            });
  }
}
