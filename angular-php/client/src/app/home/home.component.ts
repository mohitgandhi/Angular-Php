import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  public currentUser;
  public userData:any=[];
  public stackdata:any=[];
  columnDefs = [
    {field: 'username', sortable: false, filter: 'agTextColumnFilter'},
    {field: 'email', sortable: false, filter: false },
    {field: 'gender', sortable: true, filter: false}
   
];
rowData=[];
techstackList: any = [

  { id: '1', name: 'Angular'},
  { id: '2', name: 'React' },
  { id: '3', name: 'DotNet'},
  { id: '4', name: 'Sql'},
  { id: '5', name: 'Mysql'}

];
  constructor(private userService : UserService) {
    this.currentUser = localStorage.getItem('currentUser')? JSON.parse(localStorage.getItem('currentUser')) : '';
    this.userService.getloginUser(this.currentUser)
    .subscribe(  data => {
      this.userData=data;
      this.stackdata=data['techstack'];
      this.stackdata = this.stackdata.replace(/'/g, '"');
      this.stackdata= JSON.parse(this.stackdata);
      this.stackdata= this.stackdata.split(',')
     
     
    },
    error => {
      
    });
    this.userService.getregisterusers()
    .subscribe(
        data => {
          console.log(data['data']);
          this.rowData=data['data'];
          
        },
        error => {
          
        });
   }

  ngOnInit() {

  }

}
