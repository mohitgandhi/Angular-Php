import { Component, OnInit } from '@angular/core';
import { AbstractControl, ValidatorFn,FormBuilder, FormGroup, Validators,FormArray,FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { existingEmailId } from './validator/check-existing';
import { UserService } from '../_services/user.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private toastr: ToastrService
  ) {  
    }
  registerForm: FormGroup;
  techstackList: any = [

    { id:1, name: 'Angular'},
    { id:2, name: 'React' },
    { id:3, name: 'DotNet'},
    { id:4, name: 'Sql'},
    { id:5, name: 'Mysql'}

  ];
  loading = false;
  submitted = false;

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: new FormControl('', {
        validators: [Validators.required,
        this.patternValidator(/^[a-zA-Z ]*$/),
        Validators.maxLength(50)
        ],
        updateOn: 'blur'
      }),
      dob: new FormControl('', {
        validators: [Validators.required,
        this.validateDOB()
        ],
        updateOn: 'blur'
      }),
      gender: new FormControl('Male', {
        validators: [Validators.required
        ],
        updateOn: 'blur'
      }),
      techstack: this.formBuilder.array([]),
      email: new FormControl('', {
        validators: [Validators.required,
        Validators.email,
        Validators.maxLength(50),
        ],
        asyncValidators: existingEmailId.checkMail(this.userService),
        updateOn: 'blur'
      }),
      password: new FormControl('', {
        validators: [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(50)
        ],
        updateOn: 'blur'
      })
  });
  }

  onCheckboxChange(e) {

    const techstack: FormArray = this.registerForm.get('techstack') as FormArray;
    if (e.target.checked) {
      techstack.push(new FormControl(e.target.value));
    } else {
       const index = techstack.controls.findIndex(x => x.value === e.target.value);
       techstack.removeAt(index);
    }

  }

  get fval() { return this.registerForm.controls; }
  
  patternValidator(regexp: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const value = control.value;
      if (value === '') {
        return null;
      }
      return !regexp.test(value) ? { 'patternInvalid': { regexp } } : null;
    };
  }

   validateDOB(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
    var dateString = control.value;
    var dob:any = new Date(dateString);  
  
      var cur:any = new Date();
      var diff = cur-dob;
      var age = Math.floor(diff/31536000000); 
      return age<20 ? { 'ageInvalid':true} : null;
      
    
  };
  }
  

  onFormSubmit(){
    this.submitted = true;
    console.log(this.registerForm.value);
    // return for here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    this.userService.register(this.registerForm.value).subscribe(
      (data)=>{
        this.toastr.success('User Registered Successfully', 'Success');
        this.router.navigate(['/login']);
     },
      (error)=>{
        this.toastr.error('Unknown Error', 'Error');
        this.loading = false;
      }
    )

  }

}
