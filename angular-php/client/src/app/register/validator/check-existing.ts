import { AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { UserService } from '../../_services/user.service';

export class existingEmailId {
  static checkMail(userService: UserService): AsyncValidatorFn {
    return (control: AbstractControl) => {
      return userService.checkemailid(control.value).then(data => {
        if (data && data['success'] == false) {
          return null;
        } else {
          return { 'mailNotAvailable':'Email Exists' };
        }
      });
    };
  }

}
