import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/user';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient) { }

  register(user: User) {
      return this.http.post(`api/store.php`, user);
  }

  getregisterusers() {
    return this.http.get(`api/list.php`);
  }

  getloginUser(user) {
    return this.http.post('api/getBytoken.php',{'token':user});
  }

  checkemailid(emailid) {
    let self=this;
    return new Promise(function (resolve, reject) {
			self.http.post(`api/checkemail.php`,{'email':emailid})
				.subscribe((data: any) => {
					resolve(data);
				}, (err) => {
					reject(err);
				});
		});
  }
}